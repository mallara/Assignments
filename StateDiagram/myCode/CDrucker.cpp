/*
 * CDrucker.cpp
 *
 *  Created on: 22-May-2016
 *      Author: Shashank_Ravindra
 */

#include "CDrucker.h"
#include <iostream>
#include <string>
using namespace std;

CDrucker::CDrucker(print_t status, string event) :
		m_status(status), m_event(event)
{
	// TODO Auto-generated constructor stub

}

CDrucker::CDrucker()
{

}

CDrucker::~CDrucker()
{
	// TODO Auto-generated destructor stub
}

void CDrucker::printing(print_t state)
{
	string event;

	while (true)
	{
		event = getEvent();

		switch (state)
		{
		case STANDYBY:

			if (EVPRINT == event)
			{
				startTimer(1);
				state = HEATING;
				cout << "HEATING STATE" << endl;
			}

			break;

		case HEATING:

			if (EVTIMER == event && getTemperature() > TEMP)
			{
				state = HEATING;
				cout << "HEATING STATE" << endl;
			}

			else
			{
				state = PRINTING;
				cout << "PRINTING STATE" << endl;
			}

			break;

		case PRINTING:
			cout << "PRINTING STATE" << endl;
			if (EVNEXTPAGE == event && PAPERCOUNT != 0)
			{
				printOnePage();
				state = PRINTING;
				cout << "PRINTING STATE" << endl;
			}
			else if (EVPRINTFINISHED == event)
			{
				startTimer(30);
				state = PAUSE;
				cout << "PAUSE STATE" << endl;
			}
			else if (EVNEXTPAGE == event && PAPERCOUNT == 0)
			{
				showError();
				state = ERROR;
				cout << "ERROR STATE" << endl;
			}

			break;
		case ERROR:

			if (EVFILLPAPER == event)
			{
				resumePrint();
				state = PRINTING;
				cout << "PRINTING STATE" << endl;
			}
			break;
		case PAUSE:

			if (EVPRINT == event)
			{
				state = PRINTING;
				cout << "PRINTING STATE" << endl;
			}
			else if (EVTIMER == event)
			{
				state = STANDYBY;
				cout << "STANDBY STATE" << endl;
			}
			break;
		}
	}
}

string CDrucker::getEvent()
{

	cout << "Please!!! Enter the event." << endl;
	getline(cin, m_event);
	return m_event;
}

int CDrucker::startTimer(int value)
{
	return 1;
}

int CDrucker::getTemperature()
{
	int value;
	cout << "Enter Temperature!!!" << endl;
	cin >> value;
	return value;
}

bool CDrucker::printOnePage()
{
	cout << "One page Printed!!!" << endl;
	return true;
}

void CDrucker::showError()
{
	cout << "OUT OF PAPERS!!!" << endl;
}

void CDrucker::resumePrint()
{
	cout << "Print Resumed!!!" << endl;
}
