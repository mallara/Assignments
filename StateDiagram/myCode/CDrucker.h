/*
 * CDrucker.h
 *
 *  Created on: 22-May-2016
 *      Author: Shashank_Ravindra
 */

#ifndef CDRUCKER_H_
#define CDRUCKER_H_

#include <iostream>
#include <string>

using namespace std;

#define TEMP 30
#define PAPERCOUNT 20
#define EVPRINT "evprint"
#define EVTIMER "evtimer"
#define EVNEXTPAGE "evnextpage"
#define EVPRINTFINISHED "evprintfinished"
#define EVFILLPAPER "evfillpaper"

enum print_t
{
	STANDYBY, HEATING, PRINTING, ERROR, PAUSE
};

class CDrucker
{
private:
	print_t m_status;
	string m_event;

public:
	CDrucker(print_t status, string event);
	CDrucker();
	virtual ~CDrucker();
	void printing(print_t value);
	string getEvent();
	int startTimer(int value);
	int getTemperature();
	bool printOnePage();
	void showError();
	void resumePrint();
};

#endif /* CDRUCKER_H_ */
