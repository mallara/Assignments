/*
 * CAccountDatabase.h
 *
 *  Created on: 05-May-2016
 *      Author: Shashank_Ravindra
 */

#ifndef CACCOUNTDATABASE_H_
#define CACCOUNTDATABASE_H_

#include <iostream>
#include <string>
#include <list>

#include "CAccount.h"
#include "CCustomerDatabase.h"
#define MIN_BALANCE 500

class CAccountDatabase
{

private:
	list<CAccount> m_account;
	CCustomerDatabase* m_customerData;
	static int m_accountNumber;
	type m_type;

public:
	CAccountDatabase();
	virtual ~CAccountDatabase();

	void addAccount(int customerID, type accType);
	void deleteAccount(int customerID);
	CAccount* getPointerToAccount(int customerID);
	void connectToCustomerDatabase(CCustomerDatabase* database);

	void print(int id);

};

#endif /* CACCOUNTDATABASE_H_ */
