/*
 * CCustomerDatabase.cpp
 *
 *  Created on: 04-May-2016
 *      Author: Shashank_Ravindra
 */

#include "CCustomerDatabase.h"
//#include "CBank.h"
//#include "CAccountDatabase.h"

int CCustomerDatabase::m_customerID = 100;

CCustomerDatabase::CCustomerDatabase()
{
	// TODO Auto-generated constructor stub

	m_customer.clear();
}

CCustomerDatabase::~CCustomerDatabase()
{
	// TODO Auto-generated destructor stub
}

void CCustomerDatabase::addCustomer(string fname, string lname,
		long long number)
{

	CCustomer username(fname, lname, m_customerID, number);
	m_customer.push_back(username);
//	print(m_customerID);
	++m_customerID;
}

void CCustomerDatabase::deleteCustomer(string fname, string lname,
		int customerID, long long number)
{
	list<CCustomer>::iterator mylist;

	for (mylist = m_customer.begin(); mylist != m_customer.end(); mylist++)
	{
		if (customerID == mylist->getCustomerID())
		{
			m_customer.erase(mylist);
		}
	}
}

CCustomer* CCustomerDatabase::getPointerToCustomer(int customerID)
{
	CCustomer* user;
	user = NULL;
	list<CCustomer>::iterator mylist;

	for (mylist = m_customer.begin(); mylist != m_customer.end(); mylist++)
	{
		if (customerID == mylist->getCustomerID())
		{
			user = &(*mylist);

			break;
		}

	}

	return user;
}

//CAccount* CCustomerDatabase::getPointerToAccount(CAccount* new_acc)
//{
//	CAccount* acc;
//	acc = NULL;
//	list<CAccount*>::iterator mylist;
//
//	for (mylist = m_acc.begin(); mylist != m_acc.end(); mylist++)
//	{
//		if (new_acc == *mylist)
//		{
//			acc = *mylist;
////			user = &mylist->getCustomerDetails();
////			*user = mylist->getCustomerDetails();
//
//			break;
//		}
//
//	}

//return acc;
//}

void CCustomerDatabase::print(int customerID)
{
if (customerID == 0)
{
	list<CCustomer>::iterator mylist;

	for (mylist = m_customer.begin(); mylist != m_customer.end(); mylist++)
	{

		mylist->print();

	}
}
else
{
	list<CCustomer>::iterator mylist;

	for (mylist = m_customer.begin(); mylist != m_customer.end(); mylist++)
	{

		if (customerID == mylist->getCustomerID())
		{
			mylist->print();
		}

	}
}
}

int CCustomerDatabase::getCustomerID()
{
return m_customerID;
}

