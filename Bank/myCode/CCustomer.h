/*
 * CCustomer.h
 *
 *  Created on: 01-May-2016
 *      Author: Shashank_Ravindra
 */

#ifndef CCUSTOMER_H_
#define CCUSTOMER_H_

#include <iostream>
#include <string>
#include <list>

using namespace std;

class CCustomer
{

private:
	string m_firstName;
	string m_lastName;
	long long m_phoneNumber;
	int m_userID;

public:
	CCustomer(const string  firstname = "", const string lastname = "",
			int customerID = 0, const long long number = 0);
	~CCustomer();

	void setFirstName(string fname);
	void setLastName(string lname);
	void setCustomerID(int customerID);
	void setPhoneNumber(long long number);
	string getFirstName() const;
	string getLastName() const;
	string getName() const;
	long long getPhoneNumber() const;
	int getCustomerID() const;
	CCustomer getCustomerDetails();

	void print();

};

#endif /* CCUSTOMER_H_ */
