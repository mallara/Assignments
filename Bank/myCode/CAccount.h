/*
 * CAccount.h
 *
 *  Created on: 05-May-2016
 *      Author: Shashank_Ravindra
 */

#ifndef CACCOUNT_H_
#define CACCOUNT_H_

#include <iostream>
#include <string>

using namespace std;

enum type
{
	CURRENT_ACCOUNT, SAVING_ACCOUNT, FIXED_DEPOSIT, NONE
};

class CAccount
{

private:
	int m_accountNumber;
	double m_balance;
	type m_type;
	int m_ID;

public:
	CAccount(int id = 0, int accNum = 0, double balance = 0,
			type accType = NONE);
	virtual ~CAccount();

	void setAccountNumber(int number);
	void setBalance(double amount);
	double getBalance();
	int getAccountNumber();
	void setAccountType(type accType);
	type getType();
	int getID();
	void print();
	CAccount getAccountDetails();

};

#endif /* CACCOUNT_H_ */
