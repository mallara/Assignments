/*
 * CAccount.cpp
 *
 *  Created on: 05-May-2016
 *      Author: Shashank_Ravindra
 */

#include "CAccount.h"

CAccount::CAccount(int id, int accNum, double balance, type accType) :
		m_ID(id), m_accountNumber(accNum), m_balance(balance), m_type(accType)
{
	// TODO Auto-generated constructor stub

}

CAccount::~CAccount()
{
	// TODO Auto-generated destructor stub
}

void CAccount::setAccountNumber(int number)
{
	m_accountNumber = number;
}
void CAccount::setBalance(double amount)
{
	m_balance = amount;
}
double CAccount::getBalance()
{
	return m_balance;
}
int CAccount::getAccountNumber()
{
	return m_accountNumber;
}

void CAccount::setAccountType(type accType)
{

	switch (accType)
	{
	case CURRENT_ACCOUNT:
		cout << "Current Account" << endl;
		break;
	case SAVING_ACCOUNT:
		cout << "Savings Account" << endl;
		break;
	case FIXED_DEPOSIT:
		cout << "Fixed Deposit" << endl;
		break;
	default:
		cout << "Enter valid Account Type!!!" << endl;
		break;
	}
}

type CAccount::getType()
{
	cout << "Customer Account Type : ";
	switch (m_type)
	{
	case CURRENT_ACCOUNT:
		cout << "Current Account" << endl;
		break;
	case SAVING_ACCOUNT:
		cout << "Savings Account" << endl;
		break;
	case FIXED_DEPOSIT:
		cout << "Fixed Deposit" << endl;
		break;
	default:
		cout << "Enter valid Account Type!!!" << endl;
		break;
	}
	return m_type;
}

int CAccount::getID()
{
	return m_ID;
}

CAccount CAccount::getAccountDetails()
{
	CAccount acc(getID(), getAccountNumber(), getBalance(), getType());
	return acc;
}

void CAccount::print()
{

	cout << "Customer Account Details" << endl;
	cout << "Customer ID : " << getID() << endl;
	cout << "Customer AccountNumber : " << getAccountNumber() << endl;
	cout << "Customer Account Balance : " << getBalance() << endl;
	getType();
	cout << "___________________________" << endl;
	cout << endl;
}
