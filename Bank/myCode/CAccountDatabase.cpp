/*
 * CAccountDatabase.cpp
 *
 *  Created on: 05-May-2016
 *      Author: Shashank_Ravindra
 */

#include "CAccountDatabase.h"

int CAccountDatabase::m_accountNumber = 10000;

CAccountDatabase::CAccountDatabase()
{
	// TODO Auto-generated constructor stub

}

CAccountDatabase::~CAccountDatabase()
{
	// TODO Auto-generated destructor stub
}

void CAccountDatabase::connectToCustomerDatabase(CCustomerDatabase* database)
{
	m_customerData = database;
}

void CAccountDatabase::addAccount(int customerID, type accType)
{
	CCustomer* user;
	user = m_customerData->getPointerToCustomer(customerID);
	if (user != NULL)
	{
//		user->print();
		CAccount acc(customerID, m_accountNumber, MIN_BALANCE, accType);
		m_account.push_back(acc);
		++m_accountNumber;
//		acc.print();
	}
	else
	{
		cout << "You have to create a Customer Database to add an account"
				<< endl;
	}
}

void CAccountDatabase::deleteAccount(int customerID)
{
	list<CAccount>::iterator mylist;

	for (mylist = m_account.begin(); mylist != m_account.end(); mylist++)
	{

		if (customerID == mylist->getID())
		{
			m_account.erase(mylist);
		}
	}
}

//void CAccountDatabase::openAccount(string fname, string lname, int customerID,
//		int number)
//{
//
//	CCustomer user(fname, lname, customerID, number);
//	CAccount acc(customerID, m_accountNumber, MIN_BALANCE, m_type);
//	m_account.push_back(acc);
//}

void CAccountDatabase::print(int id)
{
	if (id == 0)
	{
		list<CAccount>::iterator mylist;

		for (mylist = m_account.begin(); mylist != m_account.end(); mylist++)
		{

			mylist->print();

		}
	}
	else
	{
		list<CAccount>::iterator mylist;

		for (mylist = m_account.begin(); mylist != m_account.end(); mylist++)
		{

			if (id == mylist->getID())
				mylist->print();

		}
	}
}

CAccount* CAccountDatabase::getPointerToAccount(int customerID)
{
	CAccount* acc;
	acc = NULL;
	list<CAccount>::iterator mylist;

	for (mylist = m_account.begin(); mylist != m_account.end(); mylist++)
	{
		if (customerID == mylist->getID())
		{

			acc = &(*mylist);
			break;
		}

	}
	return acc;
}

