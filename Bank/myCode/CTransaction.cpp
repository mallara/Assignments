/*
 * CTransaction.cpp
 *
 *  Created on: 08-May-2016
 *      Author: Shashank_Ravindra
 */

#include "CTransaction.h"

#include <iostream>
#include <string>
#include <ctime>
using namespace std;

//CTransaction::CTransaction()
//{
//
//}
CTransaction::CTransaction(int ID1, int ID2, int amount, string t) :
		ID1(ID1), ID2(ID2), amount(amount), time(t)
{
	// TODO Auto-generated constructor stub
//	m_userData = new CCustomer[2];
}

CTransaction::~CTransaction()
{
	// TODO Auto-generated destructor stub
}




void CTransaction::sendReceivePrint()
{
	cout << "The amount is transferred from : " << getID1();
	cout << "to : " << getID2() << "  on : "  << time <<  endl;
}

void CTransaction::print()
{
	cout << " The amount Deposited/Withdrawn from the account : " << getID1()
			<< "  is : " << getAmount() << endl;
}

//void CTransaction::setID1(int customerID)
//{
//	ID1 = customerID;
//}

int CTransaction::getID1()
{
	return ID1;
}

//void CTransaction::setID2(int customerID)
//{
//	ID2 = customerID;
//}

int CTransaction::getID2()
{
	return ID2;
}

//void CTransaction::setAmount(int amt)
//{
//	amount = amt;
//}
int CTransaction::getAmount()
{
	return amount;

}
