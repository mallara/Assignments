/*
 * CCustomerDatabase.h
 *
 *  Created on: 04-May-2016
 *      Author: Shashank_Ravindra
 */

#ifndef CCUSTOMERDATABASE_H_
#define CCUSTOMERDATABASE_H_

#include <iostream>
#include <string>
#include <list>

using namespace std;

#include "CCustomer.h"
//#include "CAccount.h"

class CCustomerDatabase
{

private:
	list<CCustomer> m_customer;
//	list<CAccount*> m_acc;
	static int m_customerID;

public:
	CCustomerDatabase();
	virtual ~CCustomerDatabase();

	void addCustomer(string fname, string lname, long long number);
	void deleteCustomer(string fname, string lname, int customerID,
			long long number);
	CCustomer* getPointerToCustomer(int customerID);
	int getCustomerID();
	void print(int customerID);
//	CAccount* getPointerToAccount(CAccount* new_acc);
//	CCustomer getCustomerDetail(int customerID);
};

#endif /* CCUSTOMERDATABASE_H_ */
