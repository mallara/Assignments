/*
 * CBank.cpp
 *
 *  Created on: 04-May-2016
 *      Author: Shashank_Ravindra
 */

#include <iostream>
#include <string>

#include "CBank.h"
using namespace std;

CBank::CBank()
{

}

CBank::~CBank()
{

}

void CBank::Customers()
{

	m_customerDatabase.addCustomer("Shashank", "Ravindra", 4917684819861);
	m_customerDatabase.addCustomer("Prajwal", "Manandar", 4917641803521);
//	m_customerDatabase.addCustomer("Peter", "Fromm", 496151168237);
//	m_customerDatabase.addCustomer("Keerthi", "Kumar", 919738729530);

	m_accountDatabase.connectToCustomerDatabase(&m_customerDatabase);
//	m_accountDatabase.addAccount(100, SAVING_ACCOUNT);
	m_accountDatabase.addAccount(100, CURRENT_ACCOUNT);
	m_accountDatabase.addAccount(101, CURRENT_ACCOUNT);

	m_transaction.connectToAccountDatabase(&m_accountDatabase);

	m_transaction.deposit(101, 1000);
	m_transaction.withdraw(100, 100);
	m_transaction.sendReceiveAmount(101, 100, 250);
	m_transaction.withdraw(100, 100);

}

void CBank::print(int value)
{

	m_customerDatabase.print(value);
	m_accountDatabase.print(value);
	m_transaction.print(value);
}

