/*
 * Cbank.h
 *
 *  Created on: 04-May-2016
 *      Author: Shashank_Ravindra
 */

#ifndef CBANK_H_
#define CBANK_H_

#include <iostream>
#include <string>

#include "CCustomerDatabase.h"
#include "CAccountDatabase.h"
#include "CTransactionDatabase.h"

using namespace std;

class CBank
{

private:

	CCustomerDatabase m_customerDatabase;
	CAccountDatabase m_accountDatabase;
	CTransactionDatabase m_transaction;

public:
	CBank();
	~CBank();

	void Customers();
	void print(int value);

};

#endif /* CBANK_H_ */
