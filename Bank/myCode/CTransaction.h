/*
 * CTransaction.h
 *
 *  Created on: 08-May-2016
 *      Author: Shashank_Ravindra
 */

#ifndef CTRANSACTION_H_
#define CTRANSACTION_H_

#include <iostream>
#include <string>
#include <list>
#include <ctime>

#include "CAccountDatabase.h"
#include "CAccount.h"

using namespace std;



class CTransaction
{

private:

//	list<CTransaction> m_transaction;
//	list<CAccount*> m_acc;
//	CCustomer m_userData;

	int ID1, ID2, amount;
	string time;

public:
	CTransaction();
	CTransaction(int ID1, int ID2, int amount, string t);
	virtual ~CTransaction();

	time_t getTime();
	void setID1(int customerID);
	int getID1();
	void setID2(int ID);
	int getID2();
	void setAmount(int amount);
	int getAmount();
	void connectToAccountDatabase(CAccountDatabase* database);
	void transactionDetails(int customerID1, int customerID2, int amount);
	void print();
	void sendReceivePrint();
};

#endif /* CTRANSACTION_H_ */
