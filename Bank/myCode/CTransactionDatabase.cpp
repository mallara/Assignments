/*
 * CTransactionDatabase.cpp
 *
 *  Created on: 14-May-2016
 *      Author: Shashank_Ravindra
 */

#include "CTransactionDatabase.h"

CTransactionDatabase::CTransactionDatabase()
{
	// TODO Auto-generated constructor stub

}

CTransactionDatabase::~CTransactionDatabase()
{
	// TODO Auto-generated destructor stub
}

void CTransactionDatabase::print(int value)
{

	list<CTransaction>::iterator mylist;

	for (mylist = m_transaction.begin(); mylist != m_transaction.end();
			mylist++)
	{
//		if (value == mylist->getID1())
//		{
		mylist->sendReceivePrint();
			mylist->print();
//		}
	}
}

void CTransactionDatabase::sendReceiveAmount(int customerID1, int customerID2,
		double amount)
{
	CAccount* accSender;
	accSender = NULL;
	CAccount* accReceiver;
	accReceiver = NULL;
	accSender = m_accData->getPointerToAccount(customerID1);
	accReceiver = m_accData->getPointerToAccount(customerID2);

	withdraw(customerID1, amount);
	deposit(customerID2, amount);

	CTransaction transaction(customerID1, customerID2, amount,
			currentDateTime());
	m_transaction.push_back(transaction);

}

bool CTransactionDatabase::deposit(int customerID, double amount)
{
	CAccount* acc;
	acc = m_accData->getPointerToAccount(customerID);
	double balance = acc->getBalance();
	if (amount < 0)
	{
		return false;
	}
	else
	{

		balance += amount;
		acc->setBalance(balance);
		CTransaction transaction(customerID, 0, amount, currentDateTime());
		m_transaction.push_back(transaction);
		cout << "The amount deposited is : " << amount << " new balance is : "
				<< acc->getBalance() << endl;
		return true;
	}

}
bool CTransactionDatabase::withdraw(int customerID, double amount)
{
	CAccount* acc;
	acc = m_accData->getPointerToAccount(customerID);
	double balance = acc->getBalance();
	if (balance < MIN_BALANCE)
	{
		cout << "Withdraw not possible" << endl;
		return false;
	}
	else
	{

		balance -= amount;
		acc->setBalance(balance);
		if (balance < MIN_BALANCE)
		{
			cout << "Withdraw Cancelled!!! Due to insufficient balance" << endl;
			balance += amount;
			acc->setBalance(balance);
			return false;
		}
		else
		{
			CTransaction transaction(customerID, 0, amount, currentDateTime());
			m_transaction.push_back(transaction);
			cout << "The amount withdrawn is : " << amount
					<< " new balance is : " << acc->getBalance() << endl;
			return true;
		}

	}
}

void CTransactionDatabase::connectToAccountDatabase(CAccountDatabase* database)
{
	m_accData = database;
}

//void CTransactionDatabase::transactionDetails(int customerID1, int customerID2,
//		int amount)
//{
////	CTransaction transaction(customerID1, customerID2, amount, time_t);
////	m_transaction.push_back(transaction);
//}
const string CTransactionDatabase::currentDateTime()
{
	time_t now = time;
	struct tm tstruct;
	char buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

	return buf;
}
