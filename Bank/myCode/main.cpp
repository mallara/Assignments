// GIT-Labor
// main.h

////////////////////////////////////////////////////////////////////////////////
// Header-Dateien
#include <iostream>		// Header f�r die Standard-IO-Objekte (z.B. cout, cin)
#include <stdlib.h>
// TODO: F�gen Sie hier weitere ben�tigte Header-Dateien der
// Standard-Bibliothek ein z.B.
// #include <string>

#include "CBank.h"
#include "CAccount.h"

using namespace std;
// Erspart den scope vor Objekte der
// C++-Standard-Bibliothek zu schreiben
// z.B. statt "std::cout" kann man "cout" schreiben

// Inkludieren Sie hier die Header-Files Ihrer Klassen, z.B.
// #include "CFraction.h"

// Hauptprogramm
// Dient als Testrahmen, von hier aus werden die Klassen aufgerufen
int main(void)
{
	// TODO: F�gen Sie ab hier Ihren Programmcode ein
	cout << "CBankReDesigned gestarted." << endl << endl;

	CBank bank;
	bank.Customers();
	bank.print(0);
//	bank.print(0);

//	CAccount add;
//	add.getBalance();
//	add.addCustomer("Abhishek", "Ravindra", 919984761590);
//	add.print(0);
	return 0;
}
