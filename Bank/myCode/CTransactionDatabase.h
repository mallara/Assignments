/*
 * CTransactionDatabase.h
 *
 *  Created on: 14-May-2016
 *      Author: Shashank_Ravindra
 */

#ifndef CTRANSACTIONDATABASE_H_
#define CTRANSACTIONDATABASE_H_

#include <iostream>
#include <string>
#include <list>
#include <time.h>

using namespace std;

#define MIN_BALANCE 500
#include "CTransaction.h"

class CTransactionDatabase
{
private:
	list<CTransaction> m_transaction;
	CAccountDatabase* m_accData;
	time_t time;

public:
	CTransactionDatabase();
	virtual ~CTransactionDatabase();
	const string currentDateTime();
	void sendReceiveAmount(int customerID1, int customerID2, double amount);
	bool deposit(int customerID, double amount);
	bool withdraw(int customerID, double amount);
	void connectToAccountDatabase(CAccountDatabase* database);
	void transactionDetails(int customerID1, int customerID2, int amount);
	void print(int value);
};

#endif /* CTRANSACTIONDATABASE_H_ */
