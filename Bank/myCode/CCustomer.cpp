/*
 * CCustomer.cpp
 *
 *  Created on: 01-May-2016
 *      Author: Shashank_Ravindra
 */

#include "CCustomer.h"

#include <iostream>
#include <string>

using namespace std;

CCustomer::CCustomer(string firstname, string lastname, int customerID,
		long long number) :
		m_firstName(firstname), m_lastName(lastname), m_userID(customerID), m_phoneNumber(
				number)
{

}

CCustomer::~CCustomer()
{

}

void CCustomer::setFirstName(string fname)
{
	m_firstName = fname;
}
void CCustomer::setLastName(string lname)
{
	m_lastName = lname;
}

string CCustomer::getFirstName() const
{
	return m_firstName;
}
string CCustomer::getLastName() const
{
	return m_lastName;
}

string CCustomer::getName() const
{
	return m_firstName + m_lastName;
}

void CCustomer::setCustomerID(int customerID)
{
	m_userID = customerID;
}
int CCustomer::getCustomerID() const
{
	return m_userID;
}

void CCustomer::setPhoneNumber(long long number)
{
	m_phoneNumber = number;
}

long long CCustomer::getPhoneNumber() const
{
	return m_phoneNumber;
}

CCustomer CCustomer::getCustomerDetails()
{
	return CCustomer(m_firstName, m_lastName, m_userID, m_phoneNumber);

}

void CCustomer::print()
{

	cout << "Customer Details" << endl;
	cout << "Customer Name : " << getName() << endl;
	cout << "Customer UserID : " << getCustomerID() << endl;
	cout << "Customer PhoneNumber : " << getPhoneNumber() << endl;
	cout << "___________________________" << endl;
	cout << endl;
	cout << endl;
}
